﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorFormMainsController : ControllerBase
    {
        private readonly VendormFormContext _context;

        public VendorFormMainsController(VendormFormContext context)
        {
            _context = context;
        }

        // GET: api/VendorFormMains
        [HttpGet]
        public IEnumerable<VendorFormMain> GetvendorFormMain()
        {
            return _context.vendorFormMain.OrderByDescending(x=> x.id);
        }

        // GET: api/VendorFormMains/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendorFormMain([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormMain = await _context.vendorFormMain.FindAsync(id);

            if (vendorFormMain == null)
            {
                return NotFound();
            }

            return Ok(vendorFormMain);
        }

        // PUT: api/VendorFormMains/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVendorFormMain([FromRoute] int id, [FromBody] VendorFormMain vendorFormMain)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendorFormMain.id)
            {
                return BadRequest();
            }

           // var vendorFormMainExist = _context.vendorFormMain.Find(id);

           // vendorFormMain.requestNo = vendorFormMainExist.requestNo;

            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");

            var configuration = builder.Build();


            var vendorConfiguration = _context.vendorFormConfiguration.SingleOrDefault(b => b.title == "Configuration");

            //send email for approval
            if (vendorFormMain.submit == "true")
            {
                vendorFormMain.status = "In Progress";
                //Finance Approver
                string emailAddress = "";


                MailMessage oMessage = new MailMessage();
                if (emailAddress != null)
                    oMessage.To.Add(new MailAddress(vendorConfiguration.toiFinanceManagerEmail));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<p>New Vendor form was submitted and waiting for your approval.</ p>");
                sb.AppendFormat("<p>Follow this <a href = '{0}{1}/{2}'>link</a></ p>", configuration["ApprovalLink"], vendorFormMain.id, "finance-manager");

                string sMessage = sb.ToString();
                SendMail(sMessage, "Submitted Vendor Request Form " + vendorFormMain.requestNo, oMessage);

            }
           
            _context.Entry(vendorFormMain).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorFormMainExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VendorFormMains
        [HttpPost]
        public async Task<IActionResult> PostVendorFormMain([FromBody] VendorFormMain vendorFormMain)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");

            var configuration = builder.Build();


            var vendorConfiguration = _context.vendorFormConfiguration.SingleOrDefault(b => b.title == "Configuration");

            vendorFormMain.requestNo = "VD" + vendorConfiguration.requestNoController.ToString("0000");

            vendorFormMain.active = "Yes";

            _context.vendorFormMain.Add(vendorFormMain);
            await _context.SaveChangesAsync();

            
            vendorConfiguration.requestNoController = vendorConfiguration.requestNoController + 1;
            _context.SaveChanges();

            //send email for approval
            if (vendorFormMain.submit == "true")
            {
                vendorFormMain.status = "In Progress";
                //Finance Approver
                string emailAddress = "";
                MailMessage oMessage = new MailMessage();
                if (emailAddress != null)
                    oMessage.To.Add(new MailAddress(vendorConfiguration.toiFinanceManagerEmail));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<p>New Vendor form was submitted and waiting for your approval.</ p>");
                sb.AppendFormat("<p>Follow this <a href = '{0}{1}/{2}'>link</a></ p>", configuration["ApprovalLink"], vendorFormMain.id, "finance-manager");

                string sMessage = sb.ToString();
                SendMail(sMessage, "Submitted Vendor Request Form " + vendorFormMain.requestNo, oMessage);

            }

            return CreatedAtAction("GetVendorFormMain", new { id = vendorFormMain.id }, vendorFormMain);
        }

        // DELETE: api/VendorFormMains/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVendorFormMain([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormMain = await _context.vendorFormMain.FindAsync(id);
            if (vendorFormMain == null)
            {
                return NotFound();
            }

            _context.vendorFormMain.Remove(vendorFormMain);
            await _context.SaveChangesAsync();

            return Ok(vendorFormMain);
        }

        private bool VendorFormMainExists(int id)
        {
            return _context.vendorFormMain.Any(e => e.id == id);
        }

        private void SendMail(String strMessage, String sSubject, MailMessage oMessageApprovers)
        {
            oMessageApprovers.From = new MailAddress("noreply@transitions.com", "noreply-");
            oMessageApprovers.Subject = sSubject;
            oMessageApprovers.Body = strMessage;
            oMessageApprovers.IsBodyHtml = true;
            SmtpClient oSMTPClient = new SmtpClient("smtp.gmail.com", 587);
            try
            {
                oSMTPClient.UseDefaultCredentials = false;
                oSMTPClient.EnableSsl = true;
                oSMTPClient.Credentials = new NetworkCredential("external.relay@transitions.com", "ydtrrputnrcimehf");
                // oSMTPClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                object userState = oMessageApprovers;
                oSMTPClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);

                oSMTPClient.SendAsync(oMessageApprovers, userState);

            }
            catch (Exception ex)
            {
            }

        }

        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //Get the Original MailMessage object
            MailMessage mail = (MailMessage)e.UserState;

            //write out the subject
            string subject = mail.Subject;

            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", subject);
            }
        }

    }
}