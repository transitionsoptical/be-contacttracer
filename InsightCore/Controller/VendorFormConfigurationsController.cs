﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorFormConfigurationsController : ControllerBase
    {
        private readonly VendormFormContext _context;

        public VendorFormConfigurationsController(VendormFormContext context)
        {
            _context = context;
        }

        // GET: api/VendorFormConfigurations
        [HttpGet]
        public IEnumerable<VendorFormConfiguration> GetvendorFormConfiguration()
        {
            return _context.vendorFormConfiguration;
        }

        // GET: api/VendorFormConfigurations/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendorFormConfiguration([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormConfiguration = await _context.vendorFormConfiguration.FindAsync(id);

            if (vendorFormConfiguration == null)
            {
                return NotFound();
            }

            return Ok(vendorFormConfiguration);
        }

        // PUT: api/VendorFormConfigurations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVendorFormConfiguration([FromRoute] int id, [FromBody] VendorFormConfiguration vendorFormConfiguration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendorFormConfiguration.id)
            {
                return BadRequest();
            }

            _context.Entry(vendorFormConfiguration).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorFormConfigurationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VendorFormConfigurations
        [HttpPost]
        public async Task<IActionResult> PostVendorFormConfiguration([FromBody] VendorFormConfiguration vendorFormConfiguration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.vendorFormConfiguration.Add(vendorFormConfiguration);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVendorFormConfiguration", new { id = vendorFormConfiguration.id }, vendorFormConfiguration);
        }

        // DELETE: api/VendorFormConfigurations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVendorFormConfiguration([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormConfiguration = await _context.vendorFormConfiguration.FindAsync(id);
            if (vendorFormConfiguration == null)
            {
                return NotFound();
            }

            _context.vendorFormConfiguration.Remove(vendorFormConfiguration);
            await _context.SaveChangesAsync();

            return Ok(vendorFormConfiguration);
        }

        private bool VendorFormConfigurationExists(int id)
        {
            return _context.vendorFormConfiguration.Any(e => e.id == id);
        }
    }
}