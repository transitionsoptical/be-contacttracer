﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorFormCreditTermsController : ControllerBase
    {
        private readonly VendormFormContext _context;

        public VendorFormCreditTermsController(VendormFormContext context)
        {
            _context = context;
        }

        // GET: api/VendorFormCreditTerms
        [HttpGet]
        public IEnumerable<VendorFormCreditTerm> GetVendorFormCreditTerm()
        {
            return _context.VendorFormCreditTerm;
        }

        // GET: api/VendorFormCreditTerms/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendorFormCreditTerm([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormCreditTerm = await _context.VendorFormCreditTerm.FindAsync(id);

            if (vendorFormCreditTerm == null)
            {
                return NotFound();
            }

            return Ok(vendorFormCreditTerm);
        }

        // PUT: api/VendorFormCreditTerms/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVendorFormCreditTerm([FromRoute] int id, [FromBody] VendorFormCreditTerm vendorFormCreditTerm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendorFormCreditTerm.id)
            {
                return BadRequest();
            }

            _context.Entry(vendorFormCreditTerm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorFormCreditTermExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VendorFormCreditTerms
        [HttpPost]
        public async Task<IActionResult> PostVendorFormCreditTerm([FromBody] VendorFormCreditTerm vendorFormCreditTerm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.VendorFormCreditTerm.Add(vendorFormCreditTerm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVendorFormCreditTerm", new { id = vendorFormCreditTerm.id }, vendorFormCreditTerm);
        }

        // DELETE: api/VendorFormCreditTerms/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVendorFormCreditTerm([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormCreditTerm = await _context.VendorFormCreditTerm.FindAsync(id);
            if (vendorFormCreditTerm == null)
            {
                return NotFound();
            }

            _context.VendorFormCreditTerm.Remove(vendorFormCreditTerm);
            await _context.SaveChangesAsync();

            return Ok(vendorFormCreditTerm);
        }

        private bool VendorFormCreditTermExists(int id)
        {
            return _context.VendorFormCreditTerm.Any(e => e.id == id);
        }
    }
}