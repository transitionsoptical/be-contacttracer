﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorFormTOSVDCountryCodesController : ControllerBase
    {
        private readonly VendormFormContext _context;

        public VendorFormTOSVDCountryCodesController(VendormFormContext context)
        {
            _context = context;
        }

        // GET: api/VendorFormTOSVDCountryCodes
        [HttpGet]
        public IEnumerable<VendorFormTOSVDCountryCode> GetvendorFormTOSVDCountryCode()
        {
            return _context.vendorFormTOSVDCountryCode.OrderBy(x=>x.country_name);
        }

        // GET: api/VendorFormTOSVDCountryCodes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendorFormTOSVDCountryCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormTOSVDCountryCode = await _context.vendorFormTOSVDCountryCode.FindAsync(id);

            if (vendorFormTOSVDCountryCode == null)
            {
                return NotFound();
            }

            return Ok(vendorFormTOSVDCountryCode);
        }

        // PUT: api/VendorFormTOSVDCountryCodes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVendorFormTOSVDCountryCode([FromRoute] int id, [FromBody] VendorFormTOSVDCountryCode vendorFormTOSVDCountryCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendorFormTOSVDCountryCode.id)
            {
                return BadRequest();
            }

            _context.Entry(vendorFormTOSVDCountryCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorFormTOSVDCountryCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VendorFormTOSVDCountryCodes
        [HttpPost]
        public async Task<IActionResult> PostVendorFormTOSVDCountryCode([FromBody] VendorFormTOSVDCountryCode vendorFormTOSVDCountryCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.vendorFormTOSVDCountryCode.Add(vendorFormTOSVDCountryCode);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVendorFormTOSVDCountryCode", new { id = vendorFormTOSVDCountryCode.id }, vendorFormTOSVDCountryCode);
        }

        // DELETE: api/VendorFormTOSVDCountryCodes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVendorFormTOSVDCountryCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormTOSVDCountryCode = await _context.vendorFormTOSVDCountryCode.FindAsync(id);
            if (vendorFormTOSVDCountryCode == null)
            {
                return NotFound();
            }

            _context.vendorFormTOSVDCountryCode.Remove(vendorFormTOSVDCountryCode);
            await _context.SaveChangesAsync();

            return Ok(vendorFormTOSVDCountryCode);
        }

        private bool VendorFormTOSVDCountryCodeExists(int id)
        {
            return _context.vendorFormTOSVDCountryCode.Any(e => e.id == id);
        }
    }
}