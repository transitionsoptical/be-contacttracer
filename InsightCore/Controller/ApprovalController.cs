﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using InsightCore.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace InsightCore.Controller
{
    [ApiController]
    public class ApprovalController : ControllerBase
    {

        private readonly VendormFormContext _context;

        public ApprovalController(VendormFormContext context)
        {
            _context = context;
        }


        [Route("api/setApprovalStatus")]
        [HttpPost()]
        public void setApprovalStatus([FromBody] Approval approval)
        {
            var approvalStatus = approval.status == "Yes" ? "Approved" : "Rejected";
            var result = _context.vendorFormMain.SingleOrDefault(b => b.id == int.Parse(approval.id));

            System.Text.StringBuilder sbRejected = new System.Text.StringBuilder();
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var vendorConfiguration = _context.vendorFormConfiguration.SingleOrDefault(b => b.title == "Configuration");

            if (result != null)
            {
                if (approval.type == "finance-manager")
                {

                    result.tosFinanceManagerDate = DateTime.Today.ToString("yyy-MM-dd");

                    if (approvalStatus.Equals("Approved"))
                    {
                        result.status = "In Progress";
                        result.tosFinanceManagerStatus = "Approved";
                        result.financeManagerAssigned = vendorConfiguration.toiFinanceManagerName;
                        result.tosFinanceManagerDate = DateTime.Today.ToString("yyy-MM-dd");
                        _context.SaveChanges();
                        //check for Strategic Partner
                        if (result.supplierType == "Strategic Partner")
                        {
                            // with AP Director
                            string emailAddress = "";
                            MailMessage oMessage = new MailMessage();
                            if (emailAddress != null)
                                oMessage.To.Add(new MailAddress(vendorConfiguration.apDirectorEmail));
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            sb.Append("<p>New Vendor form was submitted and waiting for your approval.</ p>");
                            sb.AppendFormat("<p>Follow this <a href = '{0}{1}/{2}'>link</a></ p>", configuration["ApprovalLink"], result.id, "ap-director");

                            string sMessage = sb.ToString();
                            SendMail(sMessage, "Submitted Vendor Request Form" + result.requestNo, oMessage);

                        }
                        else
                        {
                            //without AP
                            //next to PSS Personnel
                            // with AP Director
                            string emailAddress = "";
                            MailMessage oMessage = new MailMessage();
                            if (emailAddress != null)
                                oMessage.To.Add(new MailAddress(vendorConfiguration.pssPersonnelEmail));
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            sb.AppendFormat("<p>This request which has a reference <strong>{0}</strong> was approved by Finance Manager and ready to be process.", result.requestNo);
                            sb.AppendFormat("<p>Check the <a href = '{0}{1}/{2}'>link</a> for your reference.</ p>", configuration["requestLink"], result.id, "process");

                            string sMessage = sb.ToString();
                            SendMail(sMessage, "Vendor List For [" + result.requestNo + "]", oMessage);

                        }


                    }
                    else
                    {
                        result.tosFinanceManagerStatus = "Rejected by Finance Manager";
                        result.apDirectorAssigned = vendorConfiguration.apDirectorName;
                        result.apDirectorDate = DateTime.Today.ToString("yyy-MM-dd");
                        result.status = "Rejected";
                        _context.SaveChanges();
                      
                        MailMessage oMessage = new MailMessage();
                        oMessage.To.Add(new MailAddress(vendorConfiguration.pssPersonnelEmail));

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("<p>This is to inform you that your request which has a reference <strong>{0}</strong> was rejected by the Finance Manager</ p>");
                        sb.AppendFormat("<p>Check the <a href = '{0}{1}'>link</a> for your reference.</ p>", configuration["requestLinkdisp"], result.id);
                        sb.AppendFormat("<p>Approver Comment/s: <br/> {0}</ p>", approval.comment);
                        string sMessage = sb.ToString();
                        SendMail(sMessage, "Vendor Form Request - " + result.requestNo, oMessage);

                    }
                }

                else if (approval.type == "fss-aptl")
                {
                    if (approvalStatus.Equals("Approved"))
                    {
                        result.fssAPTLStatus = "Approved";
                        result.fssAPTL = vendorConfiguration.fssAptlName;
                        result.fssAPTLDateApproval = DateTime.Today.ToString("yyy-MM-dd");
                        result.status = "Completed";
                        _context.SaveChanges();
                        //check for Strategic Partner
                        MailMessage oMessage = new MailMessage();
                        oMessage.To.Add(new MailAddress(result.requestorEmail));
                        oMessage.To.Add(new MailAddress(vendorConfiguration.pssPersonnelEmail));

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.AppendFormat("<p>This is to inform you that your request which has a reference <strong>{0}</strong> was reviewed/approved by the FSS AP TL.</ p>",result.requestNo);
                        sb.AppendFormat("<p>Check the <a href = '{0}{1}'>link</a> for your reference.</ p>", configuration["requestLinkdisp"], result.id);
                        sb.AppendFormat("<p>Approver Comment/s: <br/> {0}</ p>", approval.comment);
                        string sMessage = sb.ToString();
                        SendMail(sMessage, "Vendor Form Request - " + result.requestNo, oMessage);

                    }
                    else //reject
                    {
                        result.fssAPTLStatus = "Rejected by FSS - AP/TL";
                        result.fssAPTL = vendorConfiguration.fssAptlName;
                        result.fssAPTLDateApproval = DateTime.Today.ToString("yyy-MM-dd");
                        result.status = "Rejected";
                        _context.SaveChanges();
                        //check for Strategic Partner
                        MailMessage oMessage = new MailMessage();
                        oMessage.To.Add(new MailAddress(vendorConfiguration.pssPersonnelEmail));

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("<p>This is to inform you that your request which has a reference <strong>{0}</strong> was rejected by the FSS AP TL.</ p>");
                        sb.AppendFormat("<p>Check the <a href = '{0}{1}'>link</a> for your reference.</ p>", configuration["requestLinkdisp"], result.id);
                        sb.AppendFormat("<p>Approver Comment/s: <br/> {0}</ p>", approval.comment);
                        string sMessage = sb.ToString();
                        SendMail(sMessage, "Vendor Form Request - " + result.requestNo, oMessage);

                    }


                }

                else if (approval.type == "ap-director")
                {
                    if (approvalStatus.Equals("Approved"))
                    {
                        result.apDirectorStatus = "Approved";
                        result.apDirectorAssigned = vendorConfiguration.apDirectorName;
                        result.apDirectorDate = DateTime.Today.ToString("yyy-MM-dd");
                        result.status = "In Progress";

                        string emailAddress = "";
                        MailMessage oMessage = new MailMessage();
                        if (emailAddress != null)
                            oMessage.To.Add(new MailAddress(vendorConfiguration.pssPersonnelEmail));
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.AppendFormat("<p>This request which has a reference <strong>{0}</strong> was approved by AP Director and ready to be process.", result.requestNo);
                        sb.AppendFormat("<p>Check the <a href = '{0}{1}/{2}'>link</a> for your reference.</ p>", configuration["requestLink"], result.id, "process");

                        string sMessage = sb.ToString();
                        SendMail(sMessage, "Vendor List For [" + result.requestNo + "]", oMessage);

                    }
                    else //reject
                    {
                        result.apDirectorStatus = "Rejected by AP Director";
                        result.apDirectorAssigned = vendorConfiguration.apDirectorName;
                        result.apDirectorDate = DateTime.Today.ToString("yyy-MM-dd");
                        result.status = "Rejected";
                        _context.SaveChanges();
                        //check for Strategic Partner
                        MailMessage oMessage = new MailMessage();
                        oMessage.To.Add(new MailAddress(vendorConfiguration.fssAptlEmail));

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("<p>This is to inform you that your request which has a reference <strong>{0}</strong> was rejected by the FSS AP TL.</ p>");
                        sb.AppendFormat("<p>Check the <a href = '{0}{1}'>link</a> for your reference.</ p>", configuration["requestLinkdisp"], result.id);
                        sb.AppendFormat("<p>Approver Comment/s: <br/> {0}</ p>", approval.comment);
                        string sMessage = sb.ToString();
                        SendMail(sMessage, "Vendor Form Request - " + result.requestNo, oMessage);

                    }


                }
            }
            _context.SaveChanges();
        }

        [Route("api/setSupplierCode")]
        [HttpPost()]
        public void setSupplierCode([FromBody] VendorFormMain vendorFormMain)
        {
            var result = _context.vendorFormMain.SingleOrDefault(b => b.id == vendorFormMain.id);

            result.supplier = vendorFormMain.supplier;
            _context.SaveChanges();

        }

        [Route("api/setProcess")]
        [HttpPost()]
        public void setProcess([FromBody] VendorFormMain vendorFormMain)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            var configuration = builder.Build();


            var vendorConfiguration = _context.vendorFormConfiguration.SingleOrDefault(b => b.title == "Configuration");
            var result = _context.vendorFormMain.SingleOrDefault(b => b.id == vendorFormMain.id);

            string emailAddress = "";

            result.status = "Processed";
            result.pssPersonnelAssigned = vendorConfiguration.pssPersonnelName;
            result.pssPersonnelDate = DateTime.Today.ToString("yyy-MM-dd");
            result.pssPersonnelStatus = "Processed";
            _context.SaveChanges();
            MailMessage oMessage = new MailMessage();
            if (emailAddress != null)
                oMessage.To.Add(new MailAddress(vendorConfiguration.fssAptlEmail));
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<p>New Vendor form was submitted and waiting for your approval.</ p>");
            sb.AppendFormat("<p>Follow this <a href = '{0}{1}/{2}'>link</a></ p>", configuration["ApprovalLink"], vendorFormMain.id, "fss-aptl");

            string sMessage = sb.ToString();
            SendMail(sMessage, "Vendor Form Request [" + vendorFormMain.requestNo + "] - FSS AP TL", oMessage);

        }







        private void SendMail(String strMessage, String sSubject, MailMessage oMessageApprovers)
        {
            oMessageApprovers.From = new MailAddress("noreply@transitions.com", "noreply-");
            oMessageApprovers.Subject = sSubject;
            oMessageApprovers.Body = strMessage;
            oMessageApprovers.IsBodyHtml = true;
            SmtpClient oSMTPClient = new SmtpClient("smtp.gmail.com", 587);
            try
            {
                oSMTPClient.UseDefaultCredentials = false;
                oSMTPClient.EnableSsl = true;
                oSMTPClient.Credentials = new NetworkCredential("external.relay@transitions.com", "ydtrrputnrcimehf");
                // oSMTPClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                object userState = oMessageApprovers;
                oSMTPClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);

                oSMTPClient.SendAsync(oMessageApprovers, userState);

            }
            catch (Exception ex)
            {
            }

        }

        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //Get the Original MailMessage object
            MailMessage mail = (MailMessage)e.UserState;

            //write out the subject
            string subject = mail.Subject;

            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", subject);
            }
        }



    }
}