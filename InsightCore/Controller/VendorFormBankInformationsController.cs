﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorFormBankInformationsController : ControllerBase
    {
        private readonly VendormFormContext _context;

        public VendorFormBankInformationsController(VendormFormContext context)
        {
            _context = context;
        }

        // GET: api/VendorFormBankInformations
        [HttpGet]
        public IEnumerable<VendorFormBankInformation> GetVendorFormBankInformation()
        {
            return _context.VendorFormBankInformation.OrderBy(o => o.bankName);
        }

        // GET: api/VendorFormBankInformations/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendorFormBankInformation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormBankInformation = await _context.VendorFormBankInformation.FindAsync(id);

            if (vendorFormBankInformation == null)
            {
                return NotFound();
            }

            return Ok(vendorFormBankInformation);
        }

        // PUT: api/VendorFormBankInformations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVendorFormBankInformation([FromRoute] int id, [FromBody] VendorFormBankInformation vendorFormBankInformation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendorFormBankInformation.id)
            {
                return BadRequest();
            }

            _context.Entry(vendorFormBankInformation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorFormBankInformationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VendorFormBankInformations
        [HttpPost]
        public async Task<IActionResult> PostVendorFormBankInformation([FromBody] VendorFormBankInformation vendorFormBankInformation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.VendorFormBankInformation.Add(vendorFormBankInformation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVendorFormBankInformation", new { id = vendorFormBankInformation.id }, vendorFormBankInformation);
        }

        // DELETE: api/VendorFormBankInformations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVendorFormBankInformation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vendorFormBankInformation = await _context.VendorFormBankInformation.FindAsync(id);
            if (vendorFormBankInformation == null)
            {
                return NotFound();
            }

            _context.VendorFormBankInformation.Remove(vendorFormBankInformation);
            await _context.SaveChangesAsync();

            return Ok(vendorFormBankInformation);
        }

        private bool VendorFormBankInformationExists(int id)
        {
            return _context.VendorFormBankInformation.Any(e => e.id == id);
        }
    }
}