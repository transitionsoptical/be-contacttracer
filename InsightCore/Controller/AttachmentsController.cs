﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;
using Microsoft.AspNetCore.Cors;

namespace InsightCore.Controller
{
    
    [ApiController]
    public class AttachmentsController : ControllerBase
    {
        private readonly AttachmentContext _context;

        public AttachmentsController(AttachmentContext context)
        {
            _context = context;
        }

        // GET: api/attachmentsFilter?itemID
        [Route("api/attachmentsFilter")]
        [HttpGet("{itemID}")]
        public IEnumerable<Attachment> GetAttachmentFilter([FromQuery] string itemID)
        {
			//var query = "SELECT *  FROM Attachment where itemID = '" + itemID + "'";
			IEnumerable<Attachment> items = _context.Attachment.Where(c => c.itemID == itemID).ToList<Attachment>();
			//IEnumerable<Attachment> items = _context.Attachment.FromSql(query).ToList<Attachment>();
			return items;
		}

        // PUT: api/Attachments/5
        [Route("api/[controller]")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAttachment([FromRoute] int id, [FromBody] Attachment attachment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != attachment.id)
            {
                return BadRequest();
            }

            _context.Entry(attachment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttachmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Attachments
        [Route("api/AttachmentsUpload")]
        [HttpPost]
        public async Task<IActionResult> PostAttachment([FromBody] Attachment attachment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Attachment.Add(attachment);
            await _context.SaveChangesAsync();

            return Ok(attachment);
          
        }

        // DELETE: api/Attachments/5
        [Route("api/deleteattachment/{id}")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAttachment([FromRoute] int id)
        {
            

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var attachment = await _context.Attachment.FindAsync(id);
            if (attachment != null)
            {
                _context.Attachment.Remove(attachment);
                await _context.SaveChangesAsync();
            }
            
            return Ok();
        }

        private bool AttachmentExists(int id)
        {
            return _context.Attachment.Any(e => e.id == id);
        }
    }
}