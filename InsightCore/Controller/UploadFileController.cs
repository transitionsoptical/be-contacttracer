﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;


namespace InsightCore.Controller
{
   
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        public UploadFileController( IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/UploadFile
        [Route("api/download")]
        [HttpGet("{filename}", Name = "Get")]
        public IActionResult Download([FromQuery] string filename,[FromQuery] string folderID)
        {
            if (filename == null)
                return Content("filename not present");

            var folderName = Path.Combine("Resources", "Attachment", folderID);

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                          folderName, filename);

            var net = new System.Net.WebClient();

           if ( System.IO.File.Exists(path))
            {
                var data = net.DownloadData(path);
                var content = new System.IO.MemoryStream(data);
                var contentType = "APPLICATION/octet-stream";
                return File(content, contentType, filename);
            }
            else
                return BadRequest();
        }
        
        [Route("api/[controller]")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload([FromForm] Model model)
        {
            try
            {

                var folderID = model.requestID;
                var folderName = Path.Combine("Resources", "Attachment",folderID);
               
                if (!Directory.Exists(folderName))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(folderName);
                }

                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                
                if (model.Files[0].Length > 0)
                {
                    foreach (var formFile in model.Files)
                    {
                        if (formFile.Length > 0)
                        {
                            var fileName = ContentDispositionHeaderValue.Parse(formFile.ContentDisposition).FileName.Trim('"');
                            var fullPath = Path.Combine(pathToSave, fileName);
                            var dbPath = Path.Combine(folderName, fileName);

                            using (var stream = new FileStream(fullPath, FileMode.Create))
                            {
                                formFile.CopyTo(stream);
                            }



                        }

                    }
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message +  "Internal server error");
            }
        }


		[Route("api/uploadtefile")]
		[HttpPost, DisableRequestSizeLimit]
		public IActionResult UploadTE([FromForm] ModelTe model)
		{
			try
			{

				if(model.requestID[0] == "no file" || model.Files.Count == 0)
					return Ok();


				if (model.Files[0].Length > 0)
				{
					int ctr = 0;
					foreach (var formFile in model.Files)
					{
						if (formFile.Length > 0)
						{
							var folderID = model.requestID[ctr].Split('|')[0];
							var folderName = Path.Combine("Resources", "Attachment", folderID);

							if (!Directory.Exists(folderName))
							{
								// Try to create the directory.
								DirectoryInfo di = Directory.CreateDirectory(folderName);
							}

							var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

							var fileName = model.requestID[ctr].Split('|')[1];//ContentDispositionHeaderValue.Parse(formFile.ContentDisposition).FileName.Trim('"');
							var fullPath = Path.Combine(pathToSave, fileName);
							var dbPath = Path.Combine(folderName, fileName);

							using (var stream = new FileStream(fullPath, FileMode.Create))
							{
								formFile.CopyTo(stream);
							}

							ctr++;

						}

					}
					return Ok();
				}
				else
				{
					return BadRequest();
				}

			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message + "Internal server error");
			}
		}

		// POST: api/UploadFile
		private IHostingEnvironment _hostingEnvironment;

        public class FileResult
        {
            public long Length { get; set; }
            public string Name { get; set; }
        }


        public class Model
        {
            public List<IFormFile> Files { get; set; }
            public string requestID { get; set; }

        }

		public class ModelTe
		{
			public List<IFormFile> Files { get; set; }
			public List<string> requestID { get; set; }

		}


		// DELETE: api/ApiWithActions/5
		[Route("api/deleteFile")]
        [HttpDelete("{filename,filepath}")]
        public void Delete([FromQuery] string  filename, string filepath)
        {

            var folderName = Path.Combine("Resources", "Attachment", filepath);

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                          folderName, filename);


            if (System.IO.File.Exists(path))
            {
                // If file found, delete it    
                System.IO.File.Delete(path);
                Console.WriteLine("File deleted.");
            }

        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

    }


}
