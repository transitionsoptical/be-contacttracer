﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightCore.Model;

namespace InsightCore.Model
{
    public class VendormFormContext : DbContext
    {
        public VendormFormContext(DbContextOptions<VendormFormContext> options) : base(options)
        {

        }

        public DbSet<VendorFormMain> vendorFormMain { get; set; }
        public DbSet<VendorFormTOSVDCountryCode> vendorFormTOSVDCountryCode { get; set; }
        public DbSet<InsightCore.Model.VendorFormCreditTerm> VendorFormCreditTerm { get; set; }
        public DbSet<InsightCore.Model.VendorFormBankInformation> VendorFormBankInformation { get; set; }
        public DbSet<VendorFormConfiguration> vendorFormConfiguration { get; set; }
    }
}
