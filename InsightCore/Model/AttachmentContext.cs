﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightCore.Model;

namespace InsightCore.Model
{
    public class AttachmentContext: DbContext
    {
        public AttachmentContext(DbContextOptions<AttachmentContext> options) : base(options)
        {

        }

        public DbSet<Attachment> Attachment { get; set; }
		 
    }
}
