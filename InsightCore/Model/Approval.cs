﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class Approval
    {
        public string id { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string comment { get; set; }
    }

    
}
