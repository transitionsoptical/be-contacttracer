﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class VendorFormCreditTerm
    {
        [Key]
        public int id { get; set; }

        public string entity { get; set; }
        public string terms_code { get; set; }
        public string description { get; set; }
    }
}
