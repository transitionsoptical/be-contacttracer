﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class VendorFormMain
    {
        [Key]
        public int id { get; set; }

        public string _id { get; set; }
        public string acctType { get; set; }
        public string active { get; set; }
        public string added { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string apAccount { get; set; }
        public string apCC { get; set; }
        public string apContact { get; set; }
        public string apDirectorAssigned { get; set; }
        public string apDirectorDate { get; set; }
        public string apDirectorStatus { get; set; }
        public string apSub { get; set; }
        public string approvalStatus { get; set; }
        public string attention { get; set; }
        public string attn { get; set; }
        public string autoEMTProcessing { get; set; }
        public string automaticPOReceipt { get; set; }
        public string bank { get; set; }
        public string bankAccount { get; set; }
        public string bankAccountName { get; set; }
        public string bankName { get; set; }
        public string beginDate { get; set; }
        public string branch { get; set; }
        public string buyer { get; set; }
        public string carrier { get; set; }
        public string cc { get; set; }
        public string checkForm { get; set; }
        public string city { get; set; }
        public string cocNumber { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string county { get; set; }
        public string crTerms { get; set; }
        public string created { get; set; }
        public string creditTerm { get; set; }
        public string creditTermCode { get; set; }
        public string currency { get; set; }
        public string dbNumber { get; set; }
        public string debtorNumber { get; set; }
        public string discPct { get; set; }
        public string discountTable { get; set; }
        public string edi { get; set; }
        public string emailAddress1 { get; set; }
        public string emailAddress2 { get; set; }
        public string emailAddress3 { get; set; }
        public string endDate { get; set; }
        public string entity { get; set; }
        public string ext1 { get; set; }
        public string ext2 { get; set; }
        public string faxTelex1 { get; set; }
        public string faxTelex2 { get; set; }
        public string financeManagerAssigned { get; set; }
        public string fixedPrice { get; set; }
        public int format { get; set; }
        public string fssAPTL { get; set; }
        public string fssAPTLDateApproval { get; set; }
        public string fssAPTLStatus { get; set; }
        public string inCity { get; set; }
        public string kanbanSupplier { get; set; }
        public string language { get; set; }
        public string miscCreator { get; set; }
        public string mobileNumber { get; set; }
        public string modified { get; set; }
        public string name { get; set; }
        public string natureOfTransaction { get; set; }
        public string partialOk { get; set; }


        public string paySpecification { get; set; }
        public string paymentHold { get; set; }
        public string pick { get; set; }
        public string post { get; set; }
        public string prepaymentBalance { get; set; }
        public string priceTable { get; set; }
        public string promotionGroup { get; set; }
        public string pssPersonnelAssigned { get; set; }
        public string pssPersonnelDate { get; set; }
        public string pssPersonnelStatus { get; set; }
        public string purAcct { get; set; }
        public string purchaseContract { get; set; }
        public string remarks { get; set; }
        public string requestNo { get; set; }
        public string requestType { get; set; }
        public string requestor { get; set; }
        public string requestorEmail { get; set; }
        public string sendCreditHeldSOquestor { get; set; }
        public string sendSOPrice { get; set; }
        public string shipVia { get; set; }
        public string soPriceReduction { get; set; }
        public string sortName { get; set; }
        public string state { get; set; }
        public string status { get; set; }
        public string sub { get; set; }
        public string supplier { get; set; }
        public string supplierType { get; set; }
        public string taxClass { get; set; }
        public string taxIDFederal { get; set; }
        public string taxIDMisc1 { get; set; }
        public string taxIDMisc2 { get; set; }
        public string taxIDMisc3 { get; set; }
        public string taxIDState { get; set; }
        public string taxIn { get; set; }

        public string taxReport { get; set; }
        public string taxUsage { get; set; }
        public string taxZone { get; set; }
        public string taxable { get; set; }
        public string telephone1 { get; set; }
        public string telephone2 { get; set; }
        public string temporary { get; set; }
        public string tidNotice { get; set; }
        public string tosFinanceManagerDate { get; set; }
        public string tosFinanceManagerStatus { get; set; }
        public string type { get; set; }
        public string useSOReductionPrice { get; set; }
        public string wfVendorListFollowupPSSPersonnel { get; set; }



        public string attachmentRef { get; set; }
        public string submit { get; set; }


    }
}
