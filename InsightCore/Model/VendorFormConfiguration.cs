﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class VendorFormConfiguration
    {
        [Key]
        public int id { get; set; }

        public string title { get; set; }
        public int requestNoController { get; set; }
        public string pssPersonnelName { get; set; }
        public string pssPersonnelEmail { get; set; }
        public string toiFinanceManagerName { get; set; }
        public string toiFinanceManagerEmail { get; set; }
        public string apDirectorName { get; set; }
        public string apDirectorEmail { get; set; }
        public string fssAptlName { get; set; }
        public string fssAptlEmail { get; set; }

    }
}
