﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class VendorFormBankInformation
    {
        [Key]
        public int id { get; set; }

        public string bankName { get; set; }
        public string bankCode { get; set; }
    }
}
