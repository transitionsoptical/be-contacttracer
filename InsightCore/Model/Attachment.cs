﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class Attachment
    {
        [Key]
        public int id { get; set; }

        public string filename { get; set; }
        public string itemID { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
