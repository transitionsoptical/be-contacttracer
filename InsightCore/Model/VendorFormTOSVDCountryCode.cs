﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class VendorFormTOSVDCountryCode
    {
        [Key]
        public int id { get; set; }

        public string country_code { get; set; }
        public string country_name { get; set; }
        
    }
}
